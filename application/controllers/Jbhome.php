<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
    class Jbhome extends CI_Controller { 
        public function __construct() { 
            parent::__construct(); 
            $this->load->model('mod_jbhome'); 
        } 

        public function index() { 
		if($this->session->userdata('status'))
			{
				$this->load->model('mod_jbhome'); 
				$data['judul'] = 'Data Order'; 
				$data['d_order_samuel'] = $this->mod_jbhome->order_samuel(); 
				$data['d_order_bukan_samuel'] = $this->mod_jbhome->order_bukan_samuel(); 
				$data['d_order_lebih_2'] = $this->mod_jbhome->order_lebih_2(); 
				$this->load->view('vi_jbhome', $data); 
			}else{
				redirect('jbhome/login_form'); 
			}
        }
		
        public function tambah_user() { 
            $data['judul'] = 'Tambah User'; 
            $this->load->view('tambah_user', $data); 
        } 
		
        public function list_user() { 
            $this->load->model('mod_jbhome'); 
			$data['judul'] = 'Data User'; 
			$data['d_user'] = $this->mod_jbhome->m_get_user(); 
            $this->load->view('list_user', $data); 
        } 
		
        public function tambah_user_ok() { 
            $this->load->model('mod_jbhome','',TRUE); 
            $this->mod_jbhome->m_tambah_user(); 
            redirect('jbhome/list_user'); 
        } 
		
		public function hapus_user($id) { 
            $this->load->model('mod_jbhome','',TRUE); 
            $this->mod_jbhome->m_hapus_user($id); 
            redirect('jbhome/list_user'); 
        }
		
        public function ubah_user($id) { 
            $data['judul'] = 'Ubah Data USer'; 
            $data['daftar'] = $this->mod_jbhome->get_ubah_user($id); 
            $this->load->view('ubah_user',$data); 
        } 
		
        public function pro_ubah_user() { 
            $this->load->model('mod_jbhome','',TRUE); 
            $this->mod_jbhome->m_pro_ubah_user(); 
            redirect('jbhome/list_user'); 
        }
		
        public function login_form() {  
            $this->load->view('login'); 
        } 
		
        public function login_proses() {  
            $this->load->model('mod_jbhome','',TRUE);  
			$this->mod_jbhome->cek_log();
        } 
		
		function keluar(){
			$this->session->sess_destroy();
			redirect('jbhome/index');
		}
    }
?>