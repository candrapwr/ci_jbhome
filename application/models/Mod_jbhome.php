<?php 
    class Mod_jbhome extends CI_Model { 
        public function __construct() { 
            $this->load->database(); 
        }

        function order_samuel() { 
            $query=$this->db->query("
			SELECT s.name FROM tblorder o 
			LEFT JOIN tblcustomer c ON o.cust_id=c.id 
			RIGHT JOIN tblsalesperson s ON o.salesperson_id=s.id 
			WHERE o.cust_id=4
			GROUP BY s.name
			"); 
            return $query->result(); 
        }

		function order_bukan_samuel() { 
            $query=$this->db->query("
			SELECT s.name FROM tblorder o 
			LEFT JOIN tblcustomer c ON o.cust_id=c.id 
			RIGHT JOIN tblsalesperson s ON o.salesperson_id=s.id 
			WHERE s.id NOT IN
			(SELECT salesperson_id FROM tblorder WHERE cust_id=4)
			GROUP BY s.name
			"); 
            return $query->result(); 
        }

		function order_lebih_2() { 
            $query=$this->db->query("
			SELECT s.name FROM tblorder o 
			LEFT JOIN tblcustomer c ON o.cust_id=c.id 
			RIGHT JOIN tblsalesperson s ON o.salesperson_id=s.id
			GROUP BY s.name
			HAVING COUNT(*) > 2
			"); 
            return $query->result(); 
        }
		
        function m_get_user() { 
			$query=$this->db->query("SELECT * FROM user"); 
            return $query->result(); 
        }
		
        function m_tambah_user() { 
            $data = array( 
                'username' => $this->input->post('isiusername'), 
                'password' => md5($this->input->post('isipassword'))
            ); 
            return $this->db->insert('user', $data); 
        }

        function get_ubah_user($id) { 
                $this->db->where('id_user',$id); 
                $query = $this->db->get('user'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }

        function m_pro_ubah_user() { 
            $id = $this->input->post('id_user'); 
            $data = array ( 
            'username' => $this->input->post('isiusername'), 
            'password' => md5($this->input->post('isipassword'))
            ); 
            $this->db->where('id_user',$id); 
            $this->db->update('user',$data); 
        }

        function m_hapus_user($id){ 
            $this->db->where('id_user',$id); 
            $this->db->delete('user'); 
        } 
		
        function cek_log() { 
			$where = array(
			'username' => $this->input->post('usern'),
			'password' => md5($this->input->post('pwd'))
			);
			$nilaiLog = $this->db->get_where('user',$where)->num_rows();
			if($nilaiLog > 0){
				$data_session = array(
					'namauser' => $this->input->post('usern'),
					'status' => "login"
					);
				$this->session->set_userdata($data_session);
				redirect('jbhome/index');
			}else{
				echo "<script type=\"text/javascript\">
				alert(\"Passord tau User salah !\");
				window.location.assign(\"".site_url('jbhome/login_form')."\");
				</script>";
			}				
        }		
 
    } 
?>